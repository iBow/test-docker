# Workshop 2018

## WELCOME

![](./img/logo_krementlibre.png)
![](./img/docker-kube.png) 

## K' Rément Libre

Association Vendéeen de promotion et de défense du logiciel libre, créée en 2017.

Différents événements dans l'année : conférences, ateliers, workshops, install party, projection de documentaires.

_Nous suivre_

Twitter : @krementlibre

Facebook : Page @krementlibre

Site : www.krementlibre.org


## Votre coach

![Guillaume CHÉRAMY](./img/guillaume.png)

Fervent défenseur des logiciels libre depuis **1998**

*Co-Organisateur* des _Journées du Libre en Touraine_ en 2006 et 2007

*Patron* d'une entreprise d'hébergement et d'infogérence libre depuis 2015: Entrepreneur spécialisé en #logiciellibre et #devops

*Président* de l'association _K' Rément Libre_ 

*Professeur* en écoles d'ingénieurs 


## Programme

**8h30**  
**Accueil**  
Un petit café et des viennoiseries pour vous accueillir.


**09h00 - 10h30**  
**Présentation de Docker**  
D'ou viens, Docker, le projet, les outils.
Prise en main des vms


**10h45 - 12h30**  
**Docker 101**  
* les images
* les conteneurs
* les volumes
* les ports réseau
* docker-compose

**Pause diététique**  
![](./img/raclette.jpg)

**Café et résultat du concours**
Objectif : manger toutes les baleines.

**14h00 - 16h30**  
**Les clusters**
* Docker Swarm
* Kubernetes
* Les fichiers YAML
* Déploiement et scalabilité d'une application sur Minikube
* Helm

**Fin**  
