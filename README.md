# Squelette Docker

### Configuration

TODO


### Containers
Pour entrer dans les différents containers
```bash
docker exec -it IOsteo_DB /bin/bash
docker exec -it IOsteo_Apache /bin/bash
docker exec -it IOsteo_PHP /bin/bash
docker exec -it IOsteo_Redis /bin/bash
```

Ajouter un alias dans le fichier `host` du poste de travail
```bash
sudo echo $(docker network inspect IOsteo | grep Gateway | grep -o -E '[0-9\.]+') "iosteo.local" >> /etc/hosts
```
Créer un alias pour utiliser composer
```bash
sudo echo docker-compose exec php composer >> .bashrc
docker exec -it IOsteo_PHP composer init
```

### Tools

cleanenv.sh : Supprime les containers stoppés
```bash
bin/cleanenv.sh
```
logs.sh : Affiche les logs en temps réel du containers Apache
```bash
bin/cleanenv.sh
```
stats.sh : Affiche les ressources pour chaque containers qui tourne
```bash
bin/cleanenv.sh
```
dump_sql.sh : Créé un dump de la base de données du container SQL
```bash
bin/cleanenv.sh database_name
```
restore_sql.sh : Restore/importe le dump SQL dans le container SQL 
```bash
bin/restore_sql.sh dump_file.sql
```
