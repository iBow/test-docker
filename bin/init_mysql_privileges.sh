#!/usr/bin/env bash

echo "grant all privileges on *.* to 'root'@'%' with grant option;"| docker exec -i IOsteo_DB /usr/bin/mysql -uroot -proutes
echo "SET GLOBAL max_allowed_packet =64 *1024 *1024" | docker exec -i IOsteo_DB /usr/bin/mysql -uroot -proutes
