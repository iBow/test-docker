#!/usr/bin/env bash
#title           :stats.sh
#description     :Show metrics of running containers
#author          :Jean-Mathias Gourdet
#date            :20181125
#version         :0.1
#usage           :bash stats.sh
#notes           :
#==============================================================================

docker stats --format "table {{.Container}}\t{{.Name}}\t{{.CPUPerc}}\t{{.MemUsage}}\t{{.MemPerc}}\t{{.NetIO}}\t{{.BlockIO}}\t{{.PIDs}}"
