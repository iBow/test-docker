#!/usr/bin/env bash
#title           :dump_sql.sh
#description     :Create a sql dump for the specified database.
#author          :Jean-Mathias Gourdet
#date            :20181125
#version         :0.1
#usage		     :bash dump_sql.sh database_name
#notes           :The dump will be stored inside the current directory
#==============================================================================

DATABASE=$1

docker exec IOsteo_DB /usr/bin/mysqldump --databases -uroot -proutes $DATABASE > $DATABASE.sql
