#!/usr/bin/env bash
#title           :restore_sql.sh
#description     :Import the specified sqldump inside the mysql container.
#author          :Jean-Mathias Gourdet
#date            :20181125
#version         :0.1
#usage           :bash restore_sql.sh database_name.sql
#notes           :The sql dump must be inside the current directory
#==============================================================================

DUMP=$1

cat $DUMP | docker exec -i IOsteo_DB /usr/bin/mysql -uroot -proutes
