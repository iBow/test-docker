#!/usr/bin/env bash
#title           :logs.sh
#description     :Show the container logs in real time.
#author          :Jean-Mathias Gourdet
#date            :20181125
#version         :0.1
#usage           :bash logs.sh
#notes           :
#==============================================================================

source ../.env

docker logs --tail 100 -f ${COMPOSE_PROJECT_NAME}_Apache
