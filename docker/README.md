# Environnement Docker

Ce projet contient toutes les images Docker liées aux projets Ibow.
Ces images servent à déployer des environnements de travail aussi bien pour le développement que pour la production.

### Installation de Docker
Pour l'installation de Docker cf. la page suivante: https://docs.docker.com/install/linux/docker-ce/debian/<br />
Choisir la bonne version en fonctiondu système d'exploitation.

Sur un système Linux, une fois l'installation Docker réalisée et pour facilité l'utilisation des commandes docker, docker-compose, l'utilisateur peut être ajouté au groupe docker : `sudo usermod -a -G docker <username>`

### Connexion à un registry Docker
Pour se connecter à un registry Docker et récupérer des images et des conteneurs, il est nécessaire de se connecter à ce registry.
Le registry Ibow hébergé sous gitlab est protégé en lecture. Contrairement à DockerHub, il est nécessaire de se connecter à gitlab pour pouvoir récupérer des container et des images sur celui-ci.

Connexion au registry DockerHub (nécessaire pour le push d'image et de container)
`docker login`

Connexion au registry Gitlab Ibow (nécessaire pour le pull et le push)
`docker login registry.gitlab.com`

### Les images
Il existe tout un tas d'images prêtes à l'emploi sur le docker Hub : https://hub.docker.com/<br />
Mais il est également possible de créer ses propre image à partir d'image plus basic grâce aux Dockerfile.

Pour construire l'image à partir du Dockerfile :<br/>
`docker build -t registry.gitlab.com/ibow/docker/debian -f ./Images/Debian/Dockerfile .`<br />
Ici on construit une image à partir du Dockerfile : _./Images/Debian/Dockerfile_ et qui aura pour tag _registry.gitlab.com/ibow/docker/debian_<br />
Plus d'info sur la contruction d'image ici : https://docs.docker.com/engine/reference/commandline/build/

Pour envoyer l'image créée vers le registry :<br />
`docker push registry.gitlab.com/ibow/docker/debian`<br />
Envoyer des images vers le registry permet de les utiliser pour construire d'autre images à partir de celles envoyées.

Accès au registry des images Docker d'Ibow : https://gitlab.com/iBow/Docker/container_registry

### Les containers
Un container est une instance d'une image qui tourne au sein du système de Docker.

Pour instancier un container à partir de l'image précédement créée :<br/>
`docker run --name debian registry.gitlab.com/ibow/docker/debian`

Pour instancier un container à partir de l'image précédement créée ET rentrer dedans en ligne de commande :<br/>
`docker run --name debian -ti --rm registry.gitlab.com/ibow/docker/debian`<br/>
(`--rm` aura pour effet de supprimer le container quand on s'y déconnectera)

Pour mapper un port 80 de la machine hôte avec le port 80 du container : `-p 80:80` par exemple

Plus d'info sur l'instantiation de container ici : https://docs.docker.com/engine/reference/commandline/run/

### docker-compose

### Commandes utiles

Les commandes les plus utilisées dans un contexte docker sont:

#### docker

 - Lister les conteneurs démarrés :
 `docker ps`
 - Lister tous les conteneurs :
 `docker ps -a` 
 - Obtenir la configuration d'un conteneur :
`docker inspect <conteneur>`
 - Démarrer un(des) conteneur(s) :
`docker start conteneur1 [conteneur2 conteneur3 ...]`
 - Arrêter un(des) conteneur(s) :
`docker stop conteneur1 [conteneur2 conteneur3 ...]`
 - Supprimer un(des) conteneur(s) arrêté(s) :
`docker rm conteneur1 [conteneur2 conteneur3 ...]`
 - Liste les images de conteneurs :
`docker images`
 - Supprimer une(des) image(s) :
`docker rmi image1 [image2 image3 ...]`

Pour plus d'informations voir la documentation officielle : https://docs.docker.com/engine/reference/commandline/cli/

#### docker-compose
